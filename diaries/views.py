from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import (
    TemplateView, ListView, DetailView,
    CreateView, UpdateView, DeleteView)

from .models import Diary
from .forms import DiaryForm


class TopView(TemplateView):
    template_name = 'diaries/top.html'


class DiaryListView(ListView):
    model = Diary


class DiaryDetailView(DetailView):
    model = Diary


class DiaryCreateView(CreateView):
    model = Diary
    form_class = DiaryForm

    def get_success_url(self):
        return reverse('diary_detail', kwargs={'pk': self.object.pk})


class DiaryUpdateView(UpdateView):
    model = Diary
    form_class = DiaryForm

    def get_success_url(self):
        return reverse('diary_detail', kwargs={'pk': self.object.pk})


class DiaryDeleteView(DeleteView):
    model = Diary
    success_url = reverse_lazy('diary_list')