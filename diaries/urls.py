from django.urls import path

from . import views

urlpatterns = [
    path('', views.TopView.as_view(), name='top'),
    path('diaries/', views.DiaryListView.as_view(), name='diary_list'),
    path('diaries/create/', views.DiaryCreateView.as_view(), name='diary_create'),
    path('diaries/<int:pk>/', views.DiaryDetailView.as_view(), name='diary_detail'),
    path('diaries/<int:pk>/update/', views.DiaryUpdateView.as_view(), name='diary_update'),
    path('diaries/<int:pk>/delete/', views.DiaryDeleteView.as_view(), name='diary_delete'),
]


