from django.db import models
from django.utils import timezone


class Diary(models.Model):
    title = models.CharField(max_length=256)
    detail = models.TextField(default='')
    created = models.DateField(default=timezone.now)
    image = models.ImageField(upload_to='images/')
    